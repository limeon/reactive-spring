package com.reactivespring.api.controller;

import com.reactivespring.db.dao.Product;
import com.reactivespring.db.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/product")
@AllArgsConstructor
public class ProductController {

    private ProductRepository productRepository;

    @GetMapping
    public Flux<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @PostMapping
    public Mono<Product> addProduct(@RequestBody Product product) {
        return productRepository.save(product);
    }

}

package com.reactivespring.db.repository;

import com.reactivespring.db.dao.Product;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ProductRepository extends ReactiveMongoRepository<Product, Long> {
}

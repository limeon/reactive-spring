package com.reactivespring;

import com.reactivespring.db.dao.Product;
import com.reactivespring.db.repository.ProductRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class ReactivespringApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactivespringApplication.class, args);
    }

    @Bean
    ApplicationRunner init(ProductRepository repository) {
        Object[][] data = {
                {"Test1", 2.0, 1L},
                {"Test2", 35.0, 2L},
                {"Test3", 10.0, 10L}
        };
        return args -> repository.deleteAll()
                .thenMany(Flux.just(data)
                        .map(array -> new Product((String) array[0], (Double) array[1], (Long) array[2]))
                        .flatMap(repository::save)
                ).thenMany(repository.findAll())
                .subscribe(product -> System.out.println("Saving: " + product.toString()));
    }

}

package com.reactivespring.functionalities;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

public class FilterTest {
    List<String> names = Arrays.asList("adam", "anna", "jack", "jenny");

    @Test
    void filterTest() {
        Flux<String> stringFlux = Flux.fromIterable(names)
                .filter(s -> s.startsWith("a"))
                .log();
        StepVerifier.create(stringFlux)
                .expectSubscription()
                .expectNext("adam", "anna").verifyComplete();
    }
}

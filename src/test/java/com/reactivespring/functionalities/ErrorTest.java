package com.reactivespring.functionalities;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class ErrorTest {

    @Test
    void errorHandling() {
        Flux<String> stringFlux = Flux.just("A", "B", "C")
                .concatWith(Flux.error(new RuntimeException("Error occured")))
                .concatWith(Flux.just("D"))
                .onErrorResume(e -> {
                    System.out.println("Exception is: " + e);
                    return Flux.just("default", "default1");
                });
        StepVerifier.create(stringFlux.log()).expectSubscription()
                .expectNext("A", "B", "C")
                .expectNext("default", "default1")
                .verifyComplete();
    }


    @Test
    void errorHandlingReturn() {
        Flux<String> stringFlux = Flux.just("A", "B", "C")
                .concatWith(Flux.error(new RuntimeException("Error occured")))
                .concatWith(Flux.just("D"))
                .onErrorReturn("default");
        StepVerifier.create(stringFlux.log()).expectSubscription()
                .expectNext("A", "B", "C")
                .expectNext("default")
                .verifyComplete();
    }

    @Test
    void errorHandlingMap() {
        Flux<String> stringFlux = Flux.just("A", "B", "C")
                .concatWith(Flux.error(new RuntimeException("Error occured")))
                .concatWith(Flux.just("D"))
                .onErrorMap(RuntimeException::new);
        StepVerifier.create(stringFlux.log()).expectSubscription()
                .expectNext("A", "B", "C")
                .expectError(RuntimeException.class)
                .verify();
    }

}

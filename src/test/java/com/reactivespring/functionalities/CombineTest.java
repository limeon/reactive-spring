package com.reactivespring.functionalities;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

public class CombineTest {

    @Test
    void combineUsingMerge() {
        Flux<String> flux1 = Flux.just("A", "B", "C");
        Flux<String> flux2 = Flux.just("D", "E", "F");

        Flux<String> merged = Flux.merge(flux1, flux2);

        StepVerifier.create(merged.log())
                .expectSubscription()
                .expectNext("A", "B", "C", "D", "E", "F").verifyComplete();
    }

    @Test
    void combineUsingMergeDelayed() {
        Flux<String> flux1 = Flux.just("A", "B", "C").delayElements(Duration.ofSeconds(1));
        Flux<String> flux2 = Flux.just("D", "E", "F").delayElements(Duration.ofSeconds(1));

        Flux<String> merged = Flux.merge(flux1, flux2);

        StepVerifier.create(merged.log())
                .expectSubscription()
                .expectNextCount(6)
                .expectNext("A", "B", "C", "D", "E", "F").verifyComplete();
    }

    @Test
    void combineUsingConcat() {
        Flux<String> flux1 = Flux.just("A", "B", "C").delayElements(Duration.ofSeconds(1));
        Flux<String> flux2 = Flux.just("D", "E", "F").delayElements(Duration.ofSeconds(1));

        Flux<String> merged = Flux.concat(flux1, flux2);

        StepVerifier.create(merged.log())
                .expectSubscription()
                .expectNext("A", "B", "C", "D", "E", "F").verifyComplete();
    }

    @Test
    void combineUsingZip() {
        Flux<String> flux1 = Flux.just("A", "B", "C").delayElements(Duration.ofSeconds(1));
        Flux<String> flux2 = Flux.just("D", "E", "F").delayElements(Duration.ofSeconds(1));

        Flux<String> merged = Flux.zip(flux1, flux2, String::concat);

        StepVerifier.create(merged.log())
                .expectSubscription()
                .expectNext("AD", "BE", "CF").verifyComplete();
    }
}

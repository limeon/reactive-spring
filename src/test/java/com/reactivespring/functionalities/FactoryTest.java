package com.reactivespring.functionalities;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

class FactoryTest {

    List<String> names = Arrays.asList("adam", "anna", "jack", "jenny");

    @Test
    void fluxUsingIterable() {
        Flux<String> stringFlux = Flux.fromIterable(names);
        StepVerifier.create(stringFlux.log())
                .expectNext("adam", "anna", "jack", "jenny")
                .verifyComplete();
    }

    @Test
    void fluxUsingArray() {
        String[] names = {"adam", "anna", "jack", "jenny"};
        Flux<String> stringFlux = Flux.fromArray(names);
        StepVerifier.create(stringFlux.log())
                .expectNext("adam", "anna", "jack", "jenny")
                .verifyComplete();
    }

    @Test
    void fluxUsingStream() {
        Flux<String> stringFlux = Flux.fromStream(names.stream());
        StepVerifier.create(stringFlux.log())
                .expectNext("adam", "anna", "jack", "jenny")
                .verifyComplete();
    }

    @Test
    void monoUsingJustOrEmpty() {
        Mono<String> mono = Mono.justOrEmpty(null);
        StepVerifier.create(mono.log())
                .verifyComplete();
    }

    @Test
    void monoUsingSupplier() {
        Mono<String> mono = Mono.fromSupplier(() -> "adam");
        StepVerifier.create(mono.log())
                .expectNext("adam")
                .verifyComplete();
    }

    @Test
    void fluxUsingRange() {
        Flux<Integer> integerFlux = Flux.range(1, 5);
        StepVerifier.create(integerFlux.log())
                .expectNext(1, 2, 3, 4, 5)
                .verifyComplete();
    }
}

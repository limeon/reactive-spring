package com.reactivespring.functionalities;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static reactor.core.scheduler.Schedulers.parallel;

public class MapTest {
    List<String> names = asList("adam", "anna", "jack", "jenny");

    @Test
    void mapTest() {
        Flux<String> stringFlux = Flux.fromIterable(names)
                .filter(s -> s.length() > 4)
                .map(s -> s.toUpperCase())
                .log();
        StepVerifier.create(stringFlux).expectNext("JENNY").verifyComplete();
    }

    @Test
    void flatMapBasicTest() {
        List<List<String>> list = Arrays.asList(
                Arrays.asList("a"),
                Arrays.asList("b"));
        System.out.println(list);

        System.out.println(Arrays.asList("a", "b")
                .stream()
                .flatMap(strings -> Stream.of(strings.isEmpty()))
                .collect(Collectors.toList()));
    }

    @Test
    void flatmapTest() {
        Flux<String> stringFlux = Flux.fromIterable(asList("A", "B", "C", "D", "E", "F"))
                .flatMap(s -> Flux.fromIterable(convertToList(s)))
                .log();
        StepVerifier.create(stringFlux).expectNextCount(12).verifyComplete();
    }

    @Test
    void flatmapTestParallel() {
        Flux<String> stringFlux = Flux.fromIterable(asList("A", "B", "C", "D", "E", "F"))
                .window(2)
                .flatMap(s -> s.map(this::convertToList).subscribeOn(parallel()))
                .flatMap(Flux::fromIterable)
                .log();
        StepVerifier.create(stringFlux).expectNextCount(12).verifyComplete();
    }

    @Test
    void flatmapTestParallelOrdered() {
        Flux<String> stringFlux = Flux.fromIterable(asList("A", "B", "C", "D", "E", "F"))
                .window(2)
                .flatMapSequential(s -> s.map(this::convertToList).subscribeOn(parallel()))
                .flatMap(Flux::fromIterable)
                .log();
        StepVerifier.create(stringFlux).expectNextCount(12).verifyComplete();
    }

    private List<String> convertToList(String s) {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return asList(s, "newVal");
    }
}

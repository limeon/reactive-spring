package com.reactivespring.functionalities;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class HotAndCold {

    @Test
    void coldStream() throws InterruptedException {
        Flux<String> stringFlux = Flux.just("A", "B", "C", "D", "E", "F").delayElements(Duration.ofSeconds(1));

        stringFlux.subscribe(e -> System.out.println("Subscriber 1 : " + e));

        Thread.sleep(1000L);

        stringFlux.subscribe(e -> System.out.println("Subscriber 2 : " + e));

        Thread.sleep(4000L);
    }

    @Test
    void hotStream() throws InterruptedException {
        Flux<String> stringFlux = Flux.just("A", "B", "C", "D", "E", "F").delayElements(Duration.ofSeconds(1));

        ConnectableFlux<String> connectableFlux = stringFlux.publish();
        connectableFlux.connect();

        connectableFlux.subscribe(e -> System.out.println("Subscriber 1 : " + e));

        Thread.sleep(3000L);

        connectableFlux.subscribe(e -> System.out.println("Subscriber 2 : " + e));

        Thread.sleep(4000L);
    }
}
